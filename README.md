# De Correspondent RSS Feed maker

Met dit script kun je een RSS-feed maken van de artikelen op De Correspondent.

Je hebt hiervoor Python 3.7 nodig.

## Installatie

```
git clone https://gitlab.com/finetuned/correspondent-rss-feed
cd correspondent-rss-feed
python3.7 -m venv python_env
./python_env/bin/pip3 install -r requirements.txt
```

## Gebruik

Je roept het script aan met de parameters email, wachtwoord en de bestandsnaam van het XML-bestand wat je wilt genereren. 

```
./python_env/bin/python3 correspondent_rss.py mijn@email.adres MijnWachtwoord rss.xml
```

Evenuteel als crontab in te stellen voor automatisch gebruik. Laat de file ergens plaatsen waar je vanaf het internet bij kan, en dan kun je de RSS-feed gebruiken in je RSS-lezer.
